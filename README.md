<!-- 
SPDX-FileCopyrightText: 2021 Alfred-Wegener-Institut, Helmholtz-Zentrum für Polar- und Meeresforschung (AWI)

SPDX-License-Identifier: MIT
-->

# Input temperature datasets

The repository contains sample multidimensional temperature datasets used as inputs for the biological growth model (Butzin and Pörtner, 2016; Sokolova et al., 2021).  

1. SODA data (1958-2007)

    An example dataset from the Simple Ocean Data Assimilation (SODA) retrospective analysis data set (Carton et al., 2000; Carton and Giese, 2008). The example SODA dataset provides 50 years of temperature data from 1958 to 2007 with monthly resolution. The dataset contains spatial data on a regular lon lat grid (0.5°x0.5° horizontal resolution) for the North Atlantic. We extracted temperature data for 0–580 m (0–21 vertical levels) which represent the observed depth range of cod distribution (Cohen et al., 1990). The areas with depths below 600m were blanked out. All files are in the NetCDF format. 

2. AWI Earth System Model (AWI-ESM) output 

    A sample datasets from the results of global climate model simulations (AWI-ESM coupled with an interactive ice sheet model (PISM)). Historical (1850-2014) and the representative Concentration Pathways scenarios (RCP4.5, RCP8.5) were simulated. The RCP4.5 scenario spans the years 2006-2100 (RCP4.5 scenario (2080-2100 and 2180-2200)) and was prolonged to the year 2199 with fixed greenhouse gas concentrations. The RCP8.5 scenario spans the years 2006-2100 (RCP8.5 scenario (2080-2100 and 2180-2200)) and was prolonged to the year 2215 with fixed greenhouse gas concentrations. The North Atlantic ocean temperatures are used as input for the biological growth model (Butzin and Pörtner, 2016; Sokolova et al., 2021), simulating the effect of ocean temperature change on Atlantic cod growth function. This dataset contains spatial data on a regular lon lat grid (0.5°x0.5° horizontal resolution) with monthly time step for the North Atlantic. We extracted temperature data for 0–580 m (0–21 vertical levels) which represent the observed depth range of cod distribution. The areas with depths below 600m were blanked out. All files are in the NetCDF format.


# Installation

## Step 1. Clone the repository

- via ssh (first, create [SSH key](https://docs.gitlab.com/ee/user/ssh.html))

    ~~~
    $ git clone git@gitlab.hzdr.de:awi_paleodyn/growth_model_atlantic_cod_inputs.git
    ~~~ 

- or via https

    ~~~
    $ git clone https://gitlab.hzdr.de/awi_paleodyn/growth_model_atlantic_cod_inputs.git 
    ~~~ 

    Because of the LFS label the .netcdf files in the input_data might not be downloaded by git. Please, check if the input files were downloaded correctly:

    ~~~
    $ cd ./input_data/
    $ ncdump -h <file name>.nc
    ~~~ 

    If the download via cloning didn't work, you need to download the input_data manually from the repository and replase the content of the cloned input_data folder with it.

## Step 2. Install required dependencies 

- Install `anaconda` ([documentation](https://docs.anaconda.com/anaconda/install/))
- Install required dependencies using `anaconda`:

    ```
    conda env create -f ./jupyter_notebooks/environment.yml

    ```
    or

    ```
    bash ./jupyter_notebooks/install_env.sh

    ```

## Step 3. Workflow

- Activate  created conda environment:

    ```
    conda activate analysis_viz
    
    ```


**References** </p>

Ackermann, Lars; Sokolova, Nadezhda; Gierz, Paul; Lohmann, Gerrit (2022): Historical and projected (RCP4.5, RCP8.5) temperatures simulated with the AWI-ESM earth system model in the North Atlantic. PANGAEA, https://doi.pangaea.de/10.1594/PANGAEA.943546 (dataset in review) 

Ackermann, Lars; Danek, Christopher; Gierz, Paul; Lohmann, Gerrit (2020): AMOC Recovery in a Multicentennial Scenario Using a Coupled Atmosphere‐Ocean‐Ice Sheet Model. Geophysical Research Letters, 47(16), https://doi.org/10.1029/2019GL086810

Butzin, Martin; Pörtner, Hans-Otto (2016): Thermal growth potential of Atlantic cod by the end of the 21st century. Global Change 
Biology, 22(12), 4162-4168, https://doi.org/10.1111/gcb.13375

Carton, J. A., Chepurin, G., Cao, X., and Giese, B. 2000. A simple ocean data assimilation analysis of the global upper ocean 1950–95. Part I: methodology. Journal of Physical Oceanography, 30: 294–309.<p>


Carton, J. A., and Giese, B. S. 2008. A reanalysis of ocean climate using Simple Ocean Data Assimilation (SODA). Monthly Weather Review, 136: 2999–3017.

Sokolova, Nadezhda; Butzin, Martin; Dahlke, Flemming; Werner, Karl Michael; Balting, Daniel; Lohmann, Gerrit; Pörtner, Hans-Otto (2021): Exploring the role of temperature in observed inter-population differences of Atlantic cod (Gadus morhua) growth with a 4-dimensional modelling approach. ICES Journal of Marine Science, 78(4), 1519-1529, https://doi.org/10.1093/icesjms/fsab043

Nadezhda Sokolova, Anja Rohner, Martin Butzin, Thomas Foerster, & Benjamin Wolff. (2022). Atlantic-cod-growth-model-version1.0.1 (1.0.1). Zenodo. https://doi.org/10.5281/zenodo.6705793 

</p>
